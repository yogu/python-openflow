Source: python-openflow
Section: python
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-pytest-runner,
               python3-setuptools
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://github.com/kytos/python-openflow
Vcs-Browser: https://salsa.debian.org/debian/python-openflow
Vcs-Git: https://salsa.debian.org/debian/python-openflow.git

Package: python3-openflow
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: low level library to parse OpenFlow messages
 If you want to read an OpenFlow packet from an open socket or send a message
 to an OpenFlow switch, this is your best friend. The main features are:
 high performance, latest specification compliance, short learning curve and
 free software license.
 .
 This library is part of Kytos project, a collaborative project between
 SPRACE (from São Paulo State University, Unesp) and Caltech (California
 Institute of Technology). python-openflow was developed to be used with
 Kytos controller, but feel free to use this simple and intuitive library
 in another project with another controller.
