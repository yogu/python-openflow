#!/bin/bash

set -e -u

for py in $(py3versions --supported); do
    $py -m pytest -v tests -k 'not test_unpack'
    #Disabled test_unpack needing missing ofpt_port_status.dat.
done
